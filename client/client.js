
'use strict'

const { PeerRPCClient }  = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')

const link = new Link({
  grape: 'http://127.0.0.1:30001'
})
link.start()

const peer = new PeerRPCClient(link, {})
peer.init()
// peer.request('rpc_test', { msg: 'hello' }, { timeout: 10000 }, (err, data) => {
//   if (err) {
//     console.error(err)
//     process.exit(-1)
//   }
//   console.log(data) // { msg: 'world' }
// })


const handleResponse = (messageId, dataIn) => {
  return new Promise((reject, resolve) => {
    peer.request(messageId, dataIn, { timeout: 10000 }, (err, data) => {
      if (err) {
        console.error(err)
        reject(err)
      } else {
        console.log(data.data.msg)
        resolve(data.data.msg)
      }
    })

  })
}

handleResponse('rpc_test', { msg: 'XXX' })
  .then((messageId, dataIn) => console.log(dataIn))

handleResponse('get_quotes', { msg: 'get quotes for client A' })
  .then((messageId, dataIn) => console.log(dataIn))
