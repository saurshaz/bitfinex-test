using asdf for node version management

asdf local nodejs 14.13.1

npm i -g grenache-grape


```
# boot two grape servers

grape --dp 20001 --aph 30001 --bn '127.0.0.1:20002'
grape --dp 20002 --aph 40001 --bn '127.0.0.1:20001'
```


add deps
npm install --save grenache-nodejs-http
npm install --save grenache-nodejs-link

### Simple example with grapes and rpc
create `server.js` and `client.js` for "hello world" example with grenache

in terminal 1 :: start grape 1 server `grape --dp 20001 --aph 30001 --bn '127.0.0.1:20002'`
in terminal 2 :: start grape 2 server ` grape --dp 20002 --aph 40001 --bn '127.0.0.1:20001'`
in terminal 3 :: start server `node src/server.js`
in terminal 4 :: start client `node src/client.js`, at this point server sent data is displayed in the terminal



was facing this error 

```
ERR_STREAM_WRITE_AFTER_END
```
and

```
Error: ERR_GRAPE_LOOKUP_EMPTY
    at /home/saurabh-xyz/code/projects/testws/bitfine/client/node_modules/grenache-nodejs-link/index.js:164:19
    at Object.cb (/home/saurabh-xyz/code/projects/testws/bitfine/client/node_modules/grenache-nodejs-link/node_modules/async/dist/async.js:4617:26)
    at Link.handleReply (/home/saurabh-xyz/code/projects/testws/bitfine/client/node_modules/grenache-nodejs-link/index.js:118:9)
    at /home/saurabh-xyz/code/projects/testws/bitfine/client/node_modules/grenache-nodejs-link/index.js:79:12
    at /home/saurabh-xyz/code/projects/testws/bitfine/client/node_modules/cbq/index.js:22:21
    at Array.forEach (<anonymous>)
    at CbQ.trigger (/home/saurabh-xyz/code/projects/testws/bitfine/client/node_modules/cbq/index.js:22:7)
    at Link.handleReply (/home/saurabh-xyz/code/projects/testws/bitfine/client/node_modules/grenache-nodejs-link/index.js:113:17)
    at /home/saurabh-xyz/code/projects/testws/bitfine/client/node_modules/grenache-nodejs-link/index.js:94:14
    at Request._callback (/home/saurabh-xyz/code/projects/testws/bitfine/client/node_modules/grenache-nodejs-link/index.js:44:7)
```