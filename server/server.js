
const { PeerRPCServer }  = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')

// a map of client_id and corresponding orderbooks
const ORDERBOOK = {}

// an array of all open orders
const ALL_ORDERS = [
        {
          "id": 2192502110,
          "tradingsymbol": "PIIND",
          "instrument_token": 6191105,
          "exchange": "NSE",
          "ask_price": 56,
          "bid_price": 55,
          "quantity": 1200,
        },
        {
          "id": 2192503123,
          "tradingsymbol": "BTCUSD",
          "instrument_token": 6191105,
          "exchange": "NSE",
          "ask_price": 156,
          "bid_price": 155,
          "quantity": 1400,
        },
        {
          "id": 2192503284,
          "tradingsymbol": "ETHUD",
          "instrument_token": 6191105,
          "exchange": "NSE",
          "ask_price": 256,
          "bid_price": 355,
          "quantity": 1200,
        }
      ]

module.exports = { ALL_ORDERS, ORDERBOOK }

const link = new Link({
  grape: 'http://127.0.0.1:30001'
})
link.start()

const peer = new PeerRPCServer(link, {
  timeout: 300000
})
peer.init()

const port = 1024 + Math.floor(Math.random() * 1000)
const service = peer.transport('server')
service.listen(port)

setInterval(function () {
  link.announce('rpc_test', service.port, {})
  link.announce('get_quotes', service.port, {})
    //   TODO: one for placing bids 'place_bids'
    //   TODO: one for update_order_status orders and doing the maths in ALL_ORDERS 'update_order_status' (take care of race conditions here)
    //   TODO: one for emitting ORDERBOOK[client_id] 'my_orderbook'
    // TODO: add some timer logic on client side to send 'place_bid' requests, use Math.random() to place random price,
    // and see when it matches
}, 1000)





const STOCK_DB = {}
STOCK_DB['rpc_test'] = {msg: "get all the data"}
STOCK_DB['get_quotes'] = ALL_ORDERS
const getData = (key) => {
    return STOCK_DB[key]
}




// receive requests from peers and reply
service.on('request', (rid, key, payload, handler) => {
  console.log(`** serving request for ${key}`) //  { msg: 'hello' }
  handler.reply(null, {
    "status": "success",
    "data": {msg: getData(key)},
  })
})

console.log('started server');